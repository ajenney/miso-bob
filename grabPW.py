#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 17:23:34 2019

@author: andrea
"""
# Download high resolution figures showing the current precipitable water
def grabImages(localfigdir, cdate):
    import os
    import savefromurl
    import urllib.request as request
    import re
    import shutil
    from contextlib import closing
    
    # Foldername where images will be saves
    subfolder = 'pw'
    
    # Remote location of images (subfolder YYYYMM)
    url = ('ftp://ftp.ssec.wisc.edu/pub/mtpw2/images/tpw_nrl_colors/indo/'
                 + cdate[0:6] + '/')
    
    # Read the page source
    page = request.urlopen(url)
    html = str(page.read())
    
    # Find the instances of the string comp2019MM" which are the names of the 
    # files that we want. We want to grab the most recent one
    lookstr = 'comp'
    ihit1 = [m.start() for m in re.finditer(lookstr, html)]
    ihit2 = [m.start() for m in re.finditer('.png', html)]
    newest = html[ihit1[-1] : ihit2[-1]] + '.png'
    
    # Create subfolder
    if not os.path.exists(localfigdir + cdate + '/' + subfolder): 
        os.mkdir(localfigdir + cdate + '/' + subfolder)
    
    with closing(request.urlopen(url + newest)) as r:
        with open(localfigdir + cdate + '/' + subfolder + '/' + newest, 'wb') as f:
            shutil.copyfileobj(r, f)
    
    
    
    