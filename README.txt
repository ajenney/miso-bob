Run getImages_MAIN.py after editing the local figure directory name. Uses modules 
"grabRecentSatellite.py" to download three satelitte images over the Indian Ocean,
"grabTropicalTidbits.py" to download most recent forecasts of various fields
over the Indian ocean, and "grabMJOinfo" to get two figures describing the past,
current, and forecasted future states of the MJO.  

In the directory "localfigdir" specified in the main script, a subdirectory
named as the current YYYYMMDD (UTC) will be created. In that directory, a 
subdirectory for each model, and then another directory for the satellite 
images will be created. 

Uses the following python packages:
time
os
requests
numpy
urllib
re
