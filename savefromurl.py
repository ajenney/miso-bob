#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 13:41:38 2019

@author: andrea
"""
import requests

def save(figurl, localfigdir):
    # Save as this name
    saveas = localfigdir + figurl.split('/')[-1]
    
    # Now check to see if page/file exists. If not, continue (some models
    # don't have certain variables)    
    r = requests.get(figurl, allow_redirects=True)
    if r.status_code == 404:
        return
    else:
        open(saveas, 'wb').write(r.content)