#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 10 10:40:28 2019

@author: andrea
"""

# Download images from tropical tidbits
def grabImages(figdir, cdate, hh):

    import os
    import numpy as np
    import urllib.request as request
    import re
    import savefromurl
    #%% User-defined fields
    
    # ---------- ---------- Local Information ---------- ----------    
    # Which analysis times do we want?
    # 6-hourly until +72 hours
    # 12-hourly until +5 days (120 hours)
    fctimegrab_freq   = [6, 12]
    fctimegrab_cutoff = [72, 5*24] # this is when we switch from freq 1 to freq 2
    
    # ---------- ---------- Forecast Product Description ---------- ----------
    # List models
    # GFS- Global Forecast System, ECMWF- European Center, GEM- Canadian 
    # Meteorological Centre, NAVGEM- Navy Global Environmental Model, ICON- Max 
    # Planck Institute, JMA- Japanese Meteorological Institute, WW3- WaveWatch III
    models = ['gfs','ecmwf', 'gem', 'navgem', 'icon', 'jma', 'ww3']
    
    # For the listed models, the frequency of forecast hours available
    # E.g. 6 means that there are forecast available for UTC +6, +12, +18, etc. 
    fcfreq = [6, 24, 6, 6, 3, 24, 3]
    
    # For the listed models, the frequency with which forecasts are made
    # E.g., ECMWF forecasts are made every 12 hours
    # --> This is just for our own reference
    # fchrs = [6, 12, 12, 6, 6, 12, 6]
    
    # Figures are named with this string
    # figstr <MODEL>/<FC DATE FC INIT>/<MODEL>_<VARIABLE>_india_<FCNUM>.png
    # <MODEL>               e.g., ecmwf
    # <FC DATE FC INIT>     yyyymmddhh, hh is the model initialization hour
    # <VARIABLE>            e.g., 'mslp' can be multiple, separated by underscores
    # <FCNUM>               e.g., 1,2,3... this is the fc hour (e.g., 2 = init + 6, 
    #                           for a model with fcfreq of 6 hours)
    figstr = 'https://www.tropicaltidbits.com/analysis/models/'
    
    # Fields I'm interested in
    variables = ['u200aMean',       # 5-day-mean anomalous 200 hPa u wind
                 'u850aMean',       # 5-day-mean anomalous 850 hPa u wind
                 'uv200',           # 200 hPa wind
                 'ir',              # simulated infrared brightness temp.
                 'mslp_uv850',      # MSLP and 850 hPa wind
                 'mslp_wind',       # MSLP and 10m wind
                 'mslp_pcpn',       # MSLP and precip.
                 'apcpn24',         # 24-hour accumulated precip.
                 'mslp_pwat',       # MSLP and precipitable water (PW)
                 'mslp_pwata',      # MSLP and PW normalized anomaly
                 'midRH',           # mid-level (700-400 hPa) relative humidity
                 'wavehgt',         # Significant wave height
                 'waveper']         # Wave period    
    #%% Download forecast product images
    
    # Check to see if directory with the current date exists, and create it if not
    if not os.path.exists(figdir + cdate):
        os.mkdir(figdir + cdate)
    
    # This is the string we're looking for to find the most recently downloaded 
    # model simulation time
    lookstr = 'href="'
    fmtlen  = len('YYYYMMDDHH')
    
    # The forecasts hours that we want (fc_want)
    hifreq = np.arange(0,fctimegrab_cutoff[0],fctimegrab_freq[0])
    lofreq = np.arange(fctimegrab_cutoff[0],fctimegrab_cutoff[1]+1,fctimegrab_freq[1])
    fc_want = np.concatenate((hifreq,lofreq), axis=None)
    
    # Loop through the models
    for imodel in range(len(models)):
        # Connect to the tropical tidbits server to check for the most recent
        # analysis time that is available
        url = figstr + models[imodel]
        page = request.urlopen(url)
        html = str(page.read()) # Read the page source
        page.close()
        
        # Find the instances of the string href=" which points to the urls that are
        # titled with the information we want
        ihit = [m.start() for m in re.finditer(lookstr, html)]
        fc_newest = html[ihit[-1] + len(lookstr) : ihit[-1] + len(lookstr) + fmtlen]
        
        # Determine the numbers of the figures that we want. Need to use the
        # variable fcfreq with fctimegrab_freq and fctimegrab_cutoff
        # Grab all of the figures that are for times up until the "cutoff"
        igrab = np.where(np.isin(np.arange(0, fc_want[-1]+1, fcfreq[imodel]), 
                                 fc_want))[0]+1
        
        # Make the model + initialization time directory
        foldername = figdir + cdate + '/' + models[imodel]
        foldername2 = foldername + '/' + fc_newest + '/'
        
        if not os.path.exists(foldername): 
            os.mkdir(foldername)
            os.mkdir(foldername2)
        
        # Now, download the figures for each forecast time that we want
        for ivar in range(len(variables)):
            # If we're downloading one of the 5-day mean variables (indices 0 and 1
            # of the 'variables' variable) don't loop through times and just grab 
            # the 1-5 days, 3-7 days, 6-10 day, and 11-15
            igrabmean = [1, 3, 6, 11]
            if ivar == 0 or ivar == 1:
                for itime in igrabmean:
                    figurl = (url + '/' + fc_newest + '/' + models[imodel] + '_' +
                              variables[ivar] + '_india_' + str(itime) + '.png')
                    
                    # Download figure to local directory
                    savefromurl.save(figurl, foldername2)
                
            else:
                for itime in range(len(igrab)):
                    figurl = (url + '/' + fc_newest + '/' + models[imodel] + '_' +
                        variables[ivar] + '_india_' + str(igrab[itime]) + '.png')
                    
                    # Download figure to local directory
                    savefromurl.save(figurl, foldername2)
    #%% Now, grab ocean analysis images: sst value, long-term anomaly, and
    # 7-day change (whatever that means)
    
#    foldername = figdir + cdate + '/ocean_' + hh + '/'
#    if not os.path.exists(foldername):
#        os.mkdir(foldername)
#    
#    oceanstr = 'https://www.tropicaltidbits.com/analysis/ocean/cdas-sflux_sst'
#    figtypes = ['_io_1.png','a_io_1.png','a7diff_io_1.png']
#    
#    for figname in figtypes: savefromurl.save(oceanstr + figname, foldername)