#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 12:26:01 2019

@author: andrea
"""

# Get most recent satellite imagery

def grabImages(localfigdir, cdate, strhour):
    import os
    import savefromurl
    
    # Name of the website and the different images I want
    figsource = 'http://tropic.ssec.wisc.edu/real-time/indian/images/'
    figtypes = ['wvm5','irm5','xxirmet5n'] # WV, IR, enhanced IR
    
    # Check to see if directory with the current date exists, and create it if not
    if not os.path.exists(localfigdir + cdate):
        os.mkdir(localfigdir + cdate)
        
    # Update figure directory to current date
    if not os.path.exists(localfigdir + cdate + '/' + strhour):
        os.mkdir(localfigdir + cdate + '/' + strhour)
        
    # Set directory to the current hour
    localfigdir = localfigdir + cdate + '/' + strhour + '/';
            
    #%% Main execution
            
    for figname in figtypes: savefromurl.save(figsource + figname + '.GIF', localfigdir)
