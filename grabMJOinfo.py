#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 12:15:44 2019

@author: andrea
"""

# Download images associated with the MJO phase
def grabImages(localfigdir, cdate):
    import os
    import savefromurl
    
    # Foldername where images will be saves
    subfolder = 'MJO'
    
    # Name of the website and the different images I want
    figsource = 'https://www.cpc.ncep.noaa.gov/products/precip/CWlink/MJO/'
    figtypes = ['ensplume','spatial_olrmap'] # WV, IR, enhanced IR
    
    # Check to see if directory with the current date exists, and create it if not
    if not os.path.exists(localfigdir + cdate):
        os.mkdir(localfigdir + cdate)
        
    # Update figure directory to current date
    if not os.path.exists(localfigdir + cdate + '/' + subfolder):
        os.mkdir(localfigdir + cdate + '/' + subfolder)
        
    # Set directory to the current hour
    localfigdir = localfigdir + cdate + '/' + subfolder + '/';
            
    #%% Main execution
            
    for figname in figtypes: savefromurl.save(figsource + figname + '_full.gif', localfigdir)