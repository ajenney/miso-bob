#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 12:58:34 2019

@author: andrea
"""

# Wrapper script for functions that grab images from websites and download
# to specified directory
import time
import grabRecentSatellite as grc
import grabTropicalTidbits as gtt
import grabMJOinfo as mjo
import grabPW as pw

localfigdir = '/Users/andrea/Documents/miso-bob/wx_discussions/data/'

# Current time
ctime = time.gmtime() # current time object (UTC)
cdate = time.strftime('%Y%m%d', ctime) # formatted time string (yyyymmdd)
hh    = str("{:02d}Z".format(ctime.tm_hour))

# Download most recent satellite images. Creates a folder with the current 
# YYYYMMDD (in UTC), if it does not exist, and then puts satellite images of
# IR, WV, and enhanced IR into a folder with the current UTC time within that
# subfolder (YYYYMMDD)
grc.grabImages(localfigdir, cdate, 'sat_' + hh)

# Download different model forecast products from tropical tidbits
gtt.grabImages(localfigdir, cdate, hh)

# Download MJO information
mjo.grabImages(localfigdir, cdate)

# Download high resolution current precipitable water
pw.grabImages(localfigdir, cdate)

print('All done!')
